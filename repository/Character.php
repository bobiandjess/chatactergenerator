<?php

require_once('../service/BDD.php');

$pdo = ConnectBDD();

$query = $pdo->prepare('
    
    ' 
    /* CREE LES TABLES POUR L'ENNEMY */

    . CreateTable('capacities','capacite') . ''
    . CreateTable('actions', 'action') . ''
    . CreateTable('languages', 'language') . ''
    . CreateTable('skills', 'skill') . ''
    . CreateTable('senses', 'sense') . ''

    /* CREE LA TABLE ENNEMY */

    . CreateEnnemyTable() . ''

    /* CREE LES TABLES DE JOINTURE */
    . CreateRelationTable('capRelation', 'capacities') . ''
    . CreateRelationTable('actRelation', 'actions') . ''
    . CreateRelationTable('lanRelation', 'languages') . ''
    . CreateRelationTable('skiRelation', 'skills') . ''
    . CreateRelationTable('senRelation', 'senses') . 
     '

');

$query->execute();

?>