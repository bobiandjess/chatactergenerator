<?php

include '../global.php';


// Cette fonction est la pour enlever le "s" a la fin des nom de table pour nommer une propriétés
function NameProps($props){

   return substr($props, 0, -1);
}

// Cette fonction sert a se connecter a la BDD
function ConnectBDD(){

    $BDD = new GLOBAL_VAR();

    $dataConnet = $BDD->getDsn();
    $userName = $BDD->getUser();
    $dataPassword = $BDD->getPassword();

    $pdo = new PDO($dataConnet, $userName, $dataPassword);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $pdo;
}

// Cette fonction sert a crée des tables 
function CreateTable($table, $props){

    return "
    CREATE TABLE IF NOT EXISTS ". $table ." ( id  INTEGER AUTO_INCREMENT PRIMARY KEY, ". $props ." TEXT NOT NULL );
    ";
    
}

// Cette fonction crée la table ennemy
function CreateEnnemyTable(){
    return "
    CREATE TABLE IF NOT EXISTS ennemys (
        id INTEGER AUTO_INCREMENT PRIMARY KEY,
        type_ennemy VARCHAR(255) NOT NULL,
        race VARCHAR(255) NOT NULL,
        antagonist BOOLEAN NOT NULL,
        armor_class INTEGER NOT NULL,
        health_point INTEGER NOT NULL,
        speed INTEGER NOT NULL,
        strenght INTEGER NOT NULL,
        dexterity INTEGER NOT NULL,
        intelligence INTEGER NOT NULL,
        charisma INTEGER NOT NULL,
        height INTEGER NOT NULL,
        dangerosity VARCHAR(255) NOT NULL
    
    );
    ";
}

// Cette fonction crée des tables de jointure
function CreateRelationTable($table, $joinTable, $joinTable2 = 'ennemys'){
    
    $nameProps1 = NameProps($joinTable);
    $nameProps2 = NameProps($joinTable2);
    
    return " 
    CREATE TABLE IF NOT EXISTS " .$table ." (
        id INTEGER AUTO_INCREMENT PRIMARY KEY,
        ". $nameProps1 ." INTEGER, 
        ". $nameProps2 ." INTEGER,
        FOREIGN KEY(". $nameProps1 .")REFERENCES ". $joinTable."(id),
        FOREIGN KEY(". $nameProps2 .")REFERENCES ". $joinTable2."(id)
    );
    ";
}

?>